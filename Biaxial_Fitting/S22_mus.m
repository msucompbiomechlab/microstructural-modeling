function out = S22_mus(Para,Strain,d)
r11 = Strain(:,1);
r22 = Strain(:,2);
c2 = Para(2);
c3 = Para(3);

alpha = sqrt(2.*r11.*cos(d).*cos(d)+2.*r22.*sin(d).*sin(d)+1);
v1 = 0.7;
out = v1*c2*c3.*(2-2./alpha).*exp(c3.*(alpha-1).^2).*sin(d).^2;
% out = v1*c2.*(2-2./alpha).*exp(c3.*(alpha-1).^2).*sin(d).^2;
end
