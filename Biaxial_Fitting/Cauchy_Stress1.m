function out = Cauchy_Stress1(Para,Strain)
%beta1 = Para(5);
%beta1 = pi/24;
%m2 = Para(5);
%m_dev = 0.11;
%mphi = Para(6);
%sigmaphi = 0.25;
r11 = Strain(:,1);
r22 = Strain(:,2);
r33 = (-2.*r11.*r22-r11-r22)./((2.*r11+1).*(2.*r22+1));

G11= 1./(2.*r11+1);
G22 = 1./(2.*r22+1);
G33= 1./(2.*r33+1);

n = size(Strain,1);
Stress11_neo= zeros(n,1);
Stress22_neo = zeros(n,1);
Stress33_neo = zeros(n,1);

Stress11_col = zeros(n,1);
Stress22_col = zeros(n,1);
Stress33_col = zeros(n,1);

Stress11_mus = zeros(n,1);
Stress22_mus = zeros(n,1);
parfor i = 1:n
    Stress11_neo(i) = S11_neo(Para);
    Stress22_neo(i) = S22_neo(Para);
    Stress33_neo(i) = S33_neo(Para);
 
    result =  Stress_col_truncated_new(Para(4),Para(5),Para(6),Para(7),r11(i),r22(i));
    Stress11_col(i) =  result(1);
    Stress22_col(i) =  result(2);
    Stress33_col(i) = result(3);
       
%     f_mus = @(d) S11_mus(Para,Strain(i,:),d);
%     Stress11_mus(i) = 1/(2*beta1)*integral(f_mus,-beta1,beta1);
%     g_mus = @(d) S22_mus(Para,Strain(i,:),d);
%     Stress22_mus(i)= 1/(2*beta1)*integral(g_mus,-beta1,beta1);
    Stress11_mus(i) = S11_mus(Para,Strain(i,:),0);
    Stress22_mus(i) = S22_mus(Para,Strain(i,:),0);
end

P = -(Stress33_neo+Stress33_col)./G33;

out1 = (1./G11).*(Stress11_col+Stress11_mus+Stress11_neo+P.*G11);
out2 = (1./G22).*(Stress22_col+Stress22_mus+Stress22_neo+P.*G22);
out = [out1,out2];
