%%%%%%%%%%%%%%%%%%%%%%%% fitted parameters %%%%%%%%%%%%%%%%%%%%%%%%%%%%
scale = 0.0980665;
c1 = 43.6*scale;



c2 = 30.4*scale;
c3 = 0.8526*scale;
c4 = -53.9*scale;
c5 = 37.85*scale;
%%%%%%%%%%%%%%%%%%%%%%%% prototol alpha = 1.1 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lambda2_1 = 1 + linspace(0.05,0.32,15);
lambda1_1 = 1.1*ones(1,15);
lambda3_1 = 1./(lambda1_1.*lambda2_1);
E11_1 = 0.5*(lambda1_1.^2-1);
E22_1 = 0.5*(lambda2_1.^2-1);
alpha = lambda1_1;
I1 = lambda1_1.^2 + lambda2_1.^2 + lambda3_1.^2;
W1 = c3 + c4.*(alpha-1)+2*c5.*(I1-3);
Wa = 2*c1.*(alpha-1) + 3*c2.*(alpha-1).^2 + c4*(I1-3);
S11_1 = 2*W1.*(lambda1_1.^2-lambda3_1.^2) + Wa.*lambda1_1;
S22_1 = 2*W1.*(lambda2_1.^2-lambda3_1.^2);
X_1 = [E11_1.',E22_1.'];
Y_1 = [S11_1.',S22_1.'];
%%%%%%%%%%%%%%%%%%%%%%%% prototol alpha = 1.15 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lambda2_2 = 1 + linspace(0.05,0.32,15);
lambda1_2 = 1.15*ones(1,15);
lambda3_2 = 1./(lambda1_2.*lambda2_2);
E11_2 = 0.5*(lambda1_2.^2-1);
E22_2 = 0.5*(lambda2_2.^2-1);
alpha = lambda1_2;
I1 = lambda1_2.^2 + lambda2_2.^2 + lambda3_2.^2;
W1 = c3 + c4.*(alpha-1)+2*c5.*(I1-3);
Wa = 2*c1.*(alpha-1) + 3*c2.*(alpha-1).^2 + c4*(I1-3);
S11_2 = 2*W1.*(lambda1_2.^2-lambda3_2.^2) + Wa.*lambda1_2;
S22_2 = 2*W1.*(lambda2_2.^2-lambda3_2.^2);
X_2 = [E11_2.',E22_2.'];
Y_2 = [S11_2.',S22_2.'];
%%%%%%%%%%%%%%%%%%%%%%%%% prototol alpha = 1.2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lambda2_3 = 1 + linspace(0.05,0.32,15);
lambda1_3 = 1.2*ones(1,15);
lambda3_3 = 1./(lambda1_3.*lambda2_3);
E11_3 = 0.5*(lambda1_3.^2-1);
E22_3 = 0.5*(lambda2_3.^2-1);
alpha = lambda1_3;
I1 = lambda1_3.^2 + lambda2_3.^2 + lambda3_3.^2;
W1 = c3 + c4.*(alpha-1)+2*c5.*(I1-3);
Wa = 2*c1.*(alpha-1) + 3*c2.*(alpha-1).^2 + c4*(I1-3);
S11_3 = 2*W1.*(lambda1_3.^2-lambda3_3.^2) + Wa.*lambda1_3;
S22_3 = 2*W1.*(lambda2_3.^2-lambda3_3.^2);
X_3 = [E11_3.',E22_3.'];
Y_3 = [S11_3.',S22_3.'];
%%%%%%%%%%%%%%%%%%%%%%%%% prototol equibiaxial %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
lambda2_4 = 1 + linspace(0.05,0.32,15);
lambda1_4 = 1 + linspace(0.05,0.32,15);
lambda3_4 = 1./(lambda1_4.*lambda2_4);
E11_4 = 0.5*(lambda1_4.^2-1);
E22_4 = 0.5*(lambda2_4.^2-1);
alpha = lambda1_4;
I1 = lambda1_4.^2 + lambda2_4.^2 + lambda3_4.^2;
W1 = c3 + c4.*(alpha-1)+2*c5.*(I1-3);
Wa = 2*c1.*(alpha-1) + 3*c2.*(alpha-1).^2 + c4*(I1-3);
S11_4 = 2*W1.*(lambda1_4.^2-lambda3_4.^2) + Wa.*lambda1_4;
S22_4 = 2*W1.*(lambda2_4.^2-lambda3_4.^2);
X_4 = [E11_4.',E22_4.'];
Y_4 = [S11_4.',S22_4.'];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% p0 = [1.13,0.21,11.175,58000,0.11,0.09, 0.94,0.028];
% lb = [0.1,realmin,realmin,50000,0.02,realmin,realmin,realmin];
% ub = [Inf,Inf,Inf,250000,0.22,0.1,pi/2,0.5];


% p0 = [0.5,  0.2,   12,   10,    0.12,    0.06,           0.5];

lb = [0.01,  0.1,   1.0,  0.1,   0.02,   0.01,      realmin];
ub = [10,   1,   50,  100,   0.22,    0.1,         pi/2];

a1 = (ub(1)-lb(1)).*rand(1)+ lb(1);
a2 = (ub(2)-lb(2)).*rand(1)+ lb(2);
a3 = (ub(3)-lb(3)).*rand(1)+ lb(3);
a4 = (ub(4)-lb(4)).*rand(1)+ lb(4);
a5 = (ub(5)-lb(5)).*rand(1)+ lb(5);
a6 = (ub(6)-lb(6)).*rand(1)+ lb(6);
a7 = (ub(7)-lb(7)).*rand(1)+ lb(7);
p0 = [a1, a2, a3, a4, a5, a6, a7]


options = optimset('Display','iter','MaxFunEvals',10000,'MaxIter',10000,'TolFun', 1e-9,'TolX', 1e-9);
%options = optimset('Display','iter','Algorithm', 'trust-region-reflective','MaxFunEvals',10000,'MaxIter',10000,'TolFun', 1e-9,'TolX', 1e-9);
A = []; b = []; Aeq = []; beq = [];nonlcon = [];


% g = @(p) (sum(sum(abs(Cauchy_Stress1(p,X_1)-Y_1))))+ (sum(sum(abs(Cauchy_Stress1(p,X_2)-Y_2)))) +...
%      (sum(sum(abs(Cauchy_Stress1(p,X_3)-Y_3)))) + (sum(sum(abs(Cauchy_Stress1(p,X_4)-Y_4))));
g = @(p) (sum(sum((Cauchy_Stress1(p,X_1)-Y_1).^2)))+ (sum(sum((Cauchy_Stress1(p,X_2)-Y_2).^2))) +...
     (sum(sum((Cauchy_Stress1(p,X_3)-Y_3).^2))) + (sum(sum((Cauchy_Stress1(p,X_4)-Y_4).^2)));
% % g = @(p) 3*sum((Cauchy_Stress_22(p,X_1)-Y_1(:,2)).^2)+ sum((Cauchy_Stress_22(p,X_1)-Y_1(:,2)).^2) + ...
% %          3*sum((Cauchy_Stress_11(p,X_2)-Y_2(:,1)).^2) + sum((Cauchy_Stress_22(p,X_2)-Y_2(:,2)).^2) + ...
% %          3*sum((Cauchy_Stress_11(p,X_3)-Y_3(:,1)).^2) + sum((Cauchy_Stress_22(p,X_3)-Y_3(:,2)).^2) + ...
% %          3*sum((Cauchy_Stress_11(p,X_4)-Y_4(:,1)).^2) + sum((Cauchy_Stress_22(p,X_4)-Y_4(:,2)).^2);
     
format shortG
% [pfit,fval] = fmincon(g,p0,A,b,Aeq,beq,lb,ub,nonlcon,options)
[pfit,fval,exitflag,output,lambda,grad,hessian] = fmincon(g,p0,A,b,Aeq,beq,lb,ub,nonlcon,options)

% [pfit,fval] = lsqcurvefit(g,p0,X_1,Y_1(:,1),lb,ub)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
result1 = Cauchy_Stress1(pfit,X_1);
S11_cal1 = result1(:,1);
S22_cal1 = result1(:,2);
SSE1 = sum((S11_1.'-S11_cal1).^2);
SST1 = sum((S11_1.'-mean(S11_1)).^2);
R1 = sqrt(1-SSE1/SST1);
SSE2 = sum((S22_1.'-S22_cal1).^2);
SST2 = sum((S22_1.'-mean(S22_1)).^2);
R2 = sqrt(1-SSE2/SST2);


result2 = Cauchy_Stress1(pfit,X_2);
S11_cal2 = result2(:,1);
S22_cal2 = result2(:,2);
SSE3 = sum((S11_2.'-S11_cal2).^2);
SST3 = sum((S11_2.'-mean(S11_2)).^2);
R3 = sqrt(1-SSE3/SST3);
SSE4 = sum((S22_2.'-S22_cal2).^2);
SST4 = sum((S22_2.'-mean(S22_2)).^2);
R4 = sqrt(1-SSE4/SST4);

result3 = Cauchy_Stress1(pfit,X_3);
S11_cal3 = result3(:,1);
S22_cal3 = result3(:,2);
SSE5 = sum((S11_3.'-S11_cal3).^2);
SST5 = sum((S11_3.'-mean(S11_3)).^2);
R5 = sqrt(1-SSE5/SST5);
SSE6 = sum((S22_3.'-S22_cal3).^2);
SST6 = sum((S22_3.'-mean(S22_3)).^2);
R6 = sqrt(1-SSE6/SST6);

result4 = Cauchy_Stress1(pfit,X_4);
S11_cal4 = result4(:,1);
S22_cal4 = result4(:,2);
SSE7 = sum((S11_4.'-S11_cal4).^2);
SST7 = sum((S11_4.'-mean(S11_4)).^2);
R7 = sqrt(1-SSE7/SST7);
SSE8 = sum((S22_4.'-S22_cal4).^2);
SST8 = sum((S22_4.'-mean(S22_4)).^2);
R8 = sqrt(1-SSE8/SST8);




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure 
plot(lambda1_1,S11_1,lambda2_1,S22_1,lambda1_1,S11_cal1,lambda2_1,S22_cal1)
legend('S11','S22','S11_{cal}','S22_{cal}')
title('alpha = 1.1')


figure 
plot(lambda1_2,S11_2,lambda2_2,S22_2,lambda1_2,S11_cal2,lambda2_2,S22_cal2)
legend('S11','S22','S11_{cal}','S22_{cal}')
title('alpha = 1.15')

figure 
plot(lambda1_3,S11_3,lambda2_3,S22_3,lambda1_3,S11_cal3,lambda2_3,S22_cal3)
legend('S11','S22','S11_{cal}','S22_{cal}')
title('alpha = 1.2')

figure 
plot(lambda1_4,S11_4,lambda2_4,S22_4,lambda1_4,S11_cal4,lambda2_4,S22_cal4)
legend('S11','S22','S11_{cal}','S22_{cal}')
title('equibiaxial test')

% figure 
% plot(lambda1_5,S11_5,'o','LineWidth', 2)
% hold on 
% plot(lambda2_5,S22_5,'s','LineWidth', 2)
% plot(lambda1_5,S11_cal5,'-','LineWidth', 2)
% plot(lambda2_5,S22_cal5,'--','LineWidth', 2)
% legend({'t11','t22','t11_{fit}','t22_{fit}'},'fontweight','bold','Fontsize',12)
% title('\lambda_{1}-1:\lambda_{2}-1 = 2:1','fontweight','bold','Fontsize',12)
% xlabel('stretch','Fontsize',12,'fontweight','bold')
% ylabel('Cauchy Stress (g/cm^{2})','Fontsize',12,'fontweight','bold')
% ylim([0,100])
% ax = gca;
% ax.LineWidth = 2.0;
% ax.FontWeight = 'bold'
% 
% figure 
% plot(lambda1_6,S11_6,lambda2_6,S22_6,lambda1_6,S11_cal6,lambda2_6,S22_cal6)
% legend('t11','t22','t11_{cal}','t22_{cal}')
% title('lambda1:lambda2 = 2.5:1')
