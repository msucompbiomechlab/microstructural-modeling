% Elastic Modulus is the slope of cauchy stress-engineering strain  curve.
% e = 3/19;
% E11 = 2*C2*C3*e*exp(C3*e^2) + 2*C2*C3*(e+1)*exp(C3*e^2) + 4*C2*C3^2*(e+1)*e^2*exp(C3*e^2)
syms e11;



C2 =0.36687;
C3 = 8.9279; 
sigma11 = (2*C2*C3*e11*(e11+1)*exp(C3*e11^2));
vpa(subs(sigma11,e11,3/19))

E = diff(sigma11,e11);
vpa(subs(E,e11,3/19))