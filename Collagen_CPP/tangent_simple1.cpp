#include<functional>

namespace dolfin
{

  std::vector<std::size_t> shape = {3,3,3,3};

class MyFunc2 : public Expression
{

  mutable Array<double> UX;

public:

  std::shared_ptr<const Function> U;

  MyFunc2() : Expression(shape), UX(9)
  {
  }
   
  static inline double tintegral(std::function<double(double)> f,double a, double b, int n);
  static inline double tJacobian(double xbar, double ybar, double zbar);
  static inline double tf2(double ybar,double zbar,double E11,double E22,double E33,double E12,double E21,double E13,double E31,double E23,double E32);
  static inline double tR(double ybar,double zbar);
  void eval(Array<double>& values, const Array<double>& x) const;
 
};

double  MyFunc2::tintegral(std::function<double(double)>f, double a, double b, int n)
{
   double h = (b-a)/n;
   double k = 0.0;
   for (int i = 0; i<n; i++)
   {
     k += f(a+(i+0.5)*h)*h; 
   }  
   return k;
}

double MyFunc2::tR(double ybar, double zbar)
{
   const double pi = 3.141592653589793238463;
   double a = 0.0;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;
   double sigmaphi = 0.1;
   double mphi = 0.9271;
//   double phi = arccos(sin(a)*cos(b)*sin(c)+cos(a)*cos(c));
   return 1/(2*pi)*(1/(2*sqrt(2*pi)*sigmaphi)*exp(-pow(mphi-c,2.0)/(2*pow(sigmaphi,2.0))) + 1/(2*sqrt(2*pi)*sigmaphi)*exp(-pow(pi-mphi-c,2.0)/(2*pow(sigmaphi,2.0))));

}

double MyFunc2::tJacobian(double xbar, double ybar, double zbar)
{
   const double pi = 3.141592653589793238463;

   double a = pi/24*xbar;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;
   double temp1 = cos(c)*sin(a) - cos(a)*cos(b)*sin(c);

   double d1 = (-((cos(b)*sin(c))/temp1 - (cos(a)*pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0))/((pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0) + 1.0));
   double d2 = (-((cos(c)*sin(b))/temp1 + (sin(b)*sin(c)*(sin(a)*sin(c) + cos(a)*cos(b)*cos(c)))/pow(temp1,2.0))/((pow(sin(b),2.0)*pow(sin(c),2.0))/pow(temp1,2.0) + 1.0));
   double d3 = (sin(a)*sin(b)*sin(c))/sqrt(1.0 - pow(cos(a)*cos(c) + cos(b)*sin(a)*sin(c),2.0));
   double d4 = (cos(a)*sin(c) - cos(b)*cos(c)*sin(a))/sqrt(1.0 - pow(cos(a)*cos(c) + cos(b)*sin(a)*sin(c),2.0));
   double jacobian = d1*d4 - d2*d3;
   
   return jacobian;
}

double MyFunc2::tf2(double ybar,double zbar,double E11,double E22,double E33,double E12,double E21,double E13,double E31,double E23,double E32)
{
   const double pi = 3.141592653589793238463;
   const double C2 = 5093;
   const double sigma2 = 0.07703;
   const double m2 = 0.122563;

   double a = 0.0;
   double b = pi*ybar + pi;
   double c = pi/2*zbar + pi/2;

   
   double temp1 = cos(a)*cos(c)-cos(b)*sin(a)*sin(c);
   double temp2 = cos(c)*sin(a)+cos(a)*cos(b)*sin(c);
   double temp3 = sin(b)*sin(c);
   double upper = E11*pow(temp1,2.0) + E22*pow(temp2,2.0) + E33*pow(temp3,2.0) + E12*temp1*temp2 + E21*temp1*temp2
                     + E13*temp1*temp3 + E31*temp1*temp3 + E23*temp2*temp3 + E32*temp2*temp3;
   
   std::function<double(double)> invexp = [pi,C2,sigma2,m2,upper] (double x) 
   { 

      return 1.0/sqrt(2.0*pi)/sigma2/(1-0.5*(1+erf(-m2/sigma2/sqrt(2))))*exp(-pow(m2-x,2.0)/(2.0*pow(sigma2,2.0)))*C2*(upper-x)/(1.0+2.0*x);
   };

   if (upper < 1e-6) { 
        return 0.0;}
   else {
   
         double result = tintegral(invexp, 0.0, upper, 10);

         return result;}

}




void MyFunc2::eval(Array<double>& values, const Array<double>& x) const
{

   U->eval(UX, x);
   double E11 = UX[0];
   double E12 = UX[1];
   double E13 = UX[2];
   double E21 = UX[3];
   double E22 = UX[4];
   double E23 = UX[5];
   double E31 = UX[6];
   double E32 = UX[7];
   double E33 = UX[8]; 
   /*double E11 = 0.1;
   double E12 = 0.1;
   double E13 = 0.1;
   double E21 = 0.1;
   double E22 = 0.1;
   double E23 = 0.1;
   double E31 = 0.1;
   double E32 = 0.1;
   double E33 = 0.1;*/

 
   double  C1111 = 0.0; 
   double  C1112 = 0.0;
   double  C1113 = 0.0;
   double  C1122 = 0.0;
   double  C1123 = 0.0;
   double  C1133 = 0.0;
   double  C1211 = 0.0;
   double  C1212 = 0.0;
   double  C1213 = 0.0;
   double  C1222 = 0.0;
   double  C1223 = 0.0;
   double  C1233 = 0.0;
   double  C1311 = 0.0;
   double  C1312 = 0.0;
   double  C1313 = 0.0;
   double  C1322 = 0.0;
   double  C1323 = 0.0;
   double  C1333 = 0.0;
   double  C2211 = 0.0;
   double  C2212 = 0.0;
   double  C2213 = 0.0;
   double  C2222 = 0.0;
   double  C2223 = 0.0;
   double  C2233 = 0.0;
   double  C2311 = 0.0;
   double  C2312 = 0.0;
   double  C2313 = 0.0;
   double  C2322 = 0.0;
   double  C2323 = 0.0;
   double  C2333 = 0.0;
   double  C3311 = 0.0;
   double  C3312 = 0.0;
   double  C3313 = 0.0;
   double  C3322 = 0.0;
   double  C3323 = 0.0;
   double  C3333 = 0.0;

   //MyFunc2 B;
   const double pi = 3.141592653589793238463;
   double s2 = 0.0263;
//   double weights[10] = {0.2955242247147529,0.2955242247147529,0.2692667193099963,0.2692667193099963,0.2190863625159820,0.2190863625159820,0.1494513491505806,0.1494513491505806,0.0666713443086881,0.0666713443086881};
//   double points[10] = {-0.1488743389816312,0.1488743389816312,-0.4333953941292472,0.4333953941292472,-0.6794095682990244,0.6794095682990244,-0.8650633666889845,0.8650633666889845,-0.9739065285171717,0.9739065285171717};
   double weights[30] = {0.1028526528935588,0.1028526528935588,0.1017623897484055,0.1017623897484055,0.0995934205867953,0.0995934205867953,0.0963687371746443,0.0963687371746443,0.0921225222377861,0.0921225222377861,0.0868997872010830,0.0868997872010830,0.0807558952294202,0.0807558952294202,0.0737559747377052,0.0737559747377052,0.0659742298821805,0.0659742298821805,0.0574931562176191,0.0574931562176191,0.0484026728305941,0.0484026728305941,0.0387991925696271,0.0387991925696271,0.0287847078833234,0.0287847078833234,0.0184664683110910,0.0184664683110910,0.0079681924961666,0.0079681924961666};
   double points[30] = {-0.0514718425553177,0.0514718425553177,-0.1538699136085835,0.1538699136085835,-0.2546369261678899,0.2546369261678899,-0.3527047255308781,0.3527047255308781,-0.4470337695380892,0.4470337695380892,-0.5366241481420199,0.5366241481420199,-0.6205261829892429,0.6205261829892429,-0.6978504947933158,0.6978504947933158,-0.7677774321048262,0.7677774321048262,-0.8295657623827684,0.8295657623827684,-0.8825605357920527,0.8825605357920527,-0.9262000474292743,0.9262000474292743,-0.9600218649683075,0.9600218649683075,-0.9836681232797472,0.9836681232797472,-0.9968934840746495,0.9968934840746495};
//   double weights[20] = {0.152753387130726,0.152753387130726,0.149172986472604,0.149172986472604,0.142096109318382,0.142096109318382,0.131688638449177,0.131688638449177,0.118194531961518,0.118194531961518,0.101930119817240,0.101930119817240,0.083276741576705,0.083276741576705,0.062672048334109,0.062672048334109,0.040601429800387,0.040601429800387,0.017614007139152,0.017614007139152};
//   double points[20] = {-0.076526521133497,0.076526521133497,-0.227785851141645,0.227785851141645,-0.373706088715419,0.373706088715419,-0.510867001950827,0.510867001950827,-0.636053680726515,0.636053680726515,-0.746331906460151,0.746331906460151,-0.839116971822219,0.839116971822219,-0.912234428251326,0.912234428251326,-0.963971927277914,0.963971927277914,-0.993128599185095,0.993128599185095};
   int i,j,k;
   for (i = 0; i < 30; i++)
       {
          for (j = 0; j < 30; j++)
              {
         

                     double temp0 = pi*pi/2*weights[i]*weights[j];
                     double a = 0.0;
                     double b = pi*points[i] + pi;
                     double c = pi/2*points[j] + pi/2;
                     double temp11 = cos(a)*cos(c)-cos(b)*sin(a)*sin(c);
                     double temp22 = cos(c)*sin(a)+cos(a)*cos(b)*sin(c);
                     double temp33 = sin(b)*sin(c);
                     double temp12 = (cos(a)*cos(c)-cos(b)*sin(a)*sin(c))*(cos(c)*sin(a)+cos(a)*cos(b)*sin(c));
                     double temp13 = sin(b)*sin(c)*(cos(a)*cos(c)-cos(b)*sin(a)*sin(c));
                     double temp23 = sin(b)*sin(c)*(cos(c)*sin(a)+cos(a)*cos(b)*sin(c));
 

                     double fiber_angle = tR(points[i],points[j]);
                     double waviness = tf2(points[i],points[j],E11,E22,E33,E12,E21,E13,E31,E23,E32);
                     
                     double waviness_d11 = tf2(points[i],points[j],E11+0.0001,E22,E33,E12,E21,E13,E31,E23,E32);
                     double waviness_d22 = tf2(points[i],points[j],E11,E22+0.0001,E33,E12,E21,E13,E31,E23,E32);
                     double waviness_d33 = tf2(points[i],points[j],E11,E22,E33+0.0001,E12,E21,E13,E31,E23,E32);
                     double waviness_d12 = tf2(points[i],points[j],E11,E22,E33,E12+0.0001,E21,E13,E31,E23,E32);
                     double waviness_d13 = tf2(points[i],points[j],E11,E22,E33,E12,E21,E13+0.0001,E31,E23,E32);
                     double waviness_d23 = tf2(points[i],points[j],E11,E22,E33,E12,E21,E13,E31,E23+0.0001,E32);


                     C1111 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d11-waviness)/0.0001;
                     C1211 += temp0*s2*temp12*fiber_angle*(waviness_d11-waviness)/0.0001;
                     C1311 += temp0*s2*temp13*fiber_angle*(waviness_d11-waviness)/0.0001;
                     C2211 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d11-waviness)/0.0001;
                     C2311 += temp0*s2*temp23*fiber_angle*(waviness_d11-waviness)/0.0001;
                     C3311 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d11-waviness)/0.0001;
                     
                     C1122 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d22-waviness)/0.0001;
                     C1222 += temp0*s2*temp12*fiber_angle*(waviness_d22-waviness)/0.0001;
                     C1322 += temp0*s2*temp13*fiber_angle*(waviness_d22-waviness)/0.0001;
                     C2222 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d22-waviness)/0.0001;
                     C2322 += temp0*s2*temp23*fiber_angle*(waviness_d22-waviness)/0.0001;
                     C3322 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d22-waviness)/0.0001;
                    
                     C1133 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d33-waviness)/0.0001;
                     C1233 += temp0*s2*temp12*fiber_angle*(waviness_d33-waviness)/0.0001;
                     C1333 += temp0*s2*temp13*fiber_angle*(waviness_d33-waviness)/0.0001;                     
                     C2233 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d33-waviness)/0.0001;
                     C2333 += temp0*s2*temp23*fiber_angle*(waviness_d33-waviness)/0.0001;
                     C3333 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d33-waviness)/0.0001;
                     
                     C1112 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d12-waviness)/0.0001;
                     C1212 += temp0*s2*temp12*fiber_angle*(waviness_d12-waviness)/0.0001;
                     C1312 += temp0*s2*temp13*fiber_angle*(waviness_d12-waviness)/0.0001;                   
                     C2212 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d12-waviness)/0.0001;
                     C2312 += temp0*s2*temp23*fiber_angle*(waviness_d12-waviness)/0.0001;                     
                     C3312 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d12-waviness)/0.0001;
                     
                     C1113 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d13-waviness)/0.0001;
                     C1213 += temp0*s2*temp12*fiber_angle*(waviness_d13-waviness)/0.0001;
                     C1313 += temp0*s2*temp13*fiber_angle*(waviness_d13-waviness)/0.0001;
                     C2213 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d13-waviness)/0.0001;
                     C2313 += temp0*s2*temp23*fiber_angle*(waviness_d13-waviness)/0.0001;
                     C3313 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d13-waviness)/0.0001;
                     
                     C1123 += temp0*s2*pow(temp11,2.0)*fiber_angle*(waviness_d23-waviness)/0.0001;
                     C1223 += temp0*s2*temp12*fiber_angle*(waviness_d23-waviness)/0.0001;
                     C1323 += temp0*s2*temp13*fiber_angle*(waviness_d23-waviness)/0.0001;                    
                     C2223 += temp0*s2*pow(temp22,2.0)*fiber_angle*(waviness_d23-waviness)/0.0001;
                     C2323 += temp0*s2*temp23*fiber_angle*(waviness_d23-waviness)/0.0001;                   
                     C3323 += temp0*s2*pow(temp33,2.0)*fiber_angle*(waviness_d23-waviness)/0.0001;

                                         
               
              }
       }


   /*cout << "C1111 = " << C1111 << endl;
   cout << "C1112 = " << C1112 << endl;
   cout << "C1211 = " << C1211 << endl;
   cout << "C1113 = " << C1113 << endl;
   cout << "C1311 = " << C1311 << endl;
   cout << "C1122 = " << C1122 << endl;
   cout << "C2211 = " << C2211 << endl;
   cout << "C1123 = " << C1123 << endl;
   cout << "C2311 = " << C2311 << endl;
   cout << "C1133 = " << C1133 << endl;
   cout << "C3311 = " << C3311 << endl;

   cout << "C1212 = " << C1212 << endl;
   cout << "C1213 = " << C1213 << endl;
   cout << "C1312 = " << C1312 << endl;
   cout << "C1222 = " << C1222 << endl;
   cout << "C2212 = " << C2212 << endl;
   cout << "C1223 = " << C1223 << endl;
   cout << "C2312 = " << C2312 << endl;
   cout << "C1233 = " << C1233 << endl;
   cout << "C3312 = " << C3312 << endl;
   
   cout << "C1313 = " << C1313 << endl;
   cout << "C1322 = " << C1322 << endl;
   cout << "C2213 = " << C2213 << endl;
   cout << "C1323 = " << C1323 << endl;
   cout << "C2313 = " << C2313 << endl;
   cout << "C1333 = " << C1333 << endl;
   cout << "C3313 = " << C3313 << endl;
   
   cout << "C2222 = " << C2222 << endl;
   cout << "C2223 = " << C2223 << endl;
   cout << "C2322 = " << C2322 << endl;
   cout << "C2233 = " << C2233 << endl;
   cout << "C3322 = " << C3322 << endl;
   cout << "C2323 = " << C2323 << endl;
   cout << "C2333 = " << C2333 << endl;
   cout << "C3323 = " << C3323 << endl; 
   cout << "C3333 = " << C3333 << endl;*/






 
   

   values[0] = C1111;  
   values[1] = C1112;
   values[3] = C1112;
   values[2] = C1113;
   values[6] = C1113;
   values[4] = C1122;
   values[5] = C1123;
   values[7] = C1123;
   values[8] = C1133;
   values[9] = C1211;
   values[27] = C1211;
   values[10] = C1212;
   values[28] = C1212;
   values[12] = C1212;
   values[30] = C1212;
   values[11] = C1213;
   values[15] = C1213;
   values[29] = C1213;
   values[33] = C1213;
   values[13] = C1222;
   values[31] = C1222;
   values[14] = C1223;
   values[32] = C1223;
   values[34] = C1223;
   values[16] = C1223;
   values[17] = C1233;
   values[35] = C1233;
   values[18] = C1311;
   values[54] = C1311;
   values[19] = C1312;
   values[21] = C1312;
   values[55] = C1312;
   values[57] = C1312;
   values[20] = C1313;
   values[24] = C1313;
   values[60] = C1313;
   values[56] = C1313;
   values[22] = C1322;
   values[58] = C1322;
   values[23] = C1323;
   values[25] = C1323;
   values[59] = C1323;
   values[61] = C1323;
   values[26] = C1333;
   values[62] = C1333;
   values[36] = C2211;
   values[37] = C2212;
   values[39] = C2212;
   values[38] = C2213;
   values[42] = C2213;
   values[40] = C2222;
   values[41] = C2223;
   values[43] = C2223;
   values[44] = C2233;
   values[45] = C2311;
   values[63] = C2311;
   values[46] = C2312;
   values[48] = C2312;
   values[64] = C2312;
   values[66] = C2312;
   values[47] = C2313;
   values[51] = C2313;
   values[65] = C2313;
   values[69] = C2313;
   values[49] = C2322;
   values[67] = C2322;
   values[50] = C2323;
   values[52] = C2323;
   values[68] = C2323;
   values[70] = C2323;
   values[53] = C2333;
   values[71] = C2333;
   values[72] = C3311;
   values[73] = C3312;
   values[75] = C3312;
   values[74] = C3313;
   values[78] = C3313;
   values[76] = C3322;
   values[77] = C3323;
   values[79] = C3323;
   values[80] = C3333;

}



}
